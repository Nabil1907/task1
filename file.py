class fileProduct:
    def __init__(self,Product_name,Product_size,Product_color):
        self.Product_name  = Product_name
        self.Product_size  = Product_size
        self.Product_color = Product_color

    def add_Product(self):
        f = open("FileProduct.txt","a+")
        f.write(str(self.Product_name)+" "+str(self.Product_size)+" "+str(self.Product_color) + "\n")
        f.close()
    def list_Product(self):
        f = open("FileProduct.txt","r")
        contents = f.read()
        print(contents)
    def get_Product_name(self):
        return self.Product_name
    def set_Product_name(self,Product_name):
        self.Product_name=Product_name

    def get_Product_size(self):
        return self.Product_size
    def set_Product_size(self,Product_size):
        self.Product_size=Product_size

    def get_Product_color(self):
        return self.Product_color
    def set_Product_color(self,Product_color):
        self.Product_color=Product_color
